package com.shk.carconsultmanager.exception;

public class CNoManufacturerDataException extends RuntimeException{
    public CNoManufacturerDataException(String msg, Throwable t) {
        super(msg, t); // 부모의 생성자에 msg, t 값을 인자로 주며 생성
    }

    public CNoManufacturerDataException(String msg) {
        super(msg); // 부모의 생성자에 msg 값을 인자로 주며 생성
    }

    public CNoManufacturerDataException() {
        super(); // 부모의 생성자를 생성
    }
}
