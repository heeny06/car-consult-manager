package com.shk.carconsultmanager.repository;

import com.shk.carconsultmanager.entity.Manufacturer;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ManufacturerRepository extends JpaRepository<Manufacturer, Long> {
}
