package com.shk.carconsultmanager.repository;

import com.shk.carconsultmanager.entity.CarGrade;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CarGradeRepository extends JpaRepository<CarGrade, Long> {
}
