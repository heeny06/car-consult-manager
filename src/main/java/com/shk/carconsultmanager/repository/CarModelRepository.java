package com.shk.carconsultmanager.repository;

import com.shk.carconsultmanager.entity.CarModel;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CarModelRepository extends JpaRepository<CarModel, Long> {
}
