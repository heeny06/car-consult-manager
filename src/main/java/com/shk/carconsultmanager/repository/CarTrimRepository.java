package com.shk.carconsultmanager.repository;

import com.shk.carconsultmanager.entity.CarTrim;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface CarTrimRepository extends JpaRepository<CarTrim, Long> {
    List<CarTrim> findAllByCarGrade_CarModel_Id(Long modelId);
}
