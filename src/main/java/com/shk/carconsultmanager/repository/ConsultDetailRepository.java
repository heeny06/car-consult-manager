package com.shk.carconsultmanager.repository;

import com.shk.carconsultmanager.entity.ConsultDetail;
import org.springframework.data.jpa.repository.JpaRepository;

import java.time.LocalDateTime;
import java.util.List;

public interface ConsultDetailRepository extends JpaRepository<ConsultDetail, Long> {
    List<ConsultDetail> findAllByDateRequestGreaterThanEqualAndDateRequestLessThanEqualOrderByIdDesc(
            LocalDateTime dateStart,
            LocalDateTime dateEnd
    );
}
