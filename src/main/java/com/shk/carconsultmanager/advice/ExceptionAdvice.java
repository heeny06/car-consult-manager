package com.shk.carconsultmanager.advice;

import com.shk.carconsultmanager.enums.ResultCode;
import com.shk.carconsultmanager.exception.CMissingDataException;
import com.shk.carconsultmanager.exception.CNoManufacturerDataException;
import com.shk.carconsultmanager.model.CommonResult;
import com.shk.carconsultmanager.service.ResponseService;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import javax.servlet.http.HttpServletRequest;

@RestControllerAdvice
public class ExceptionAdvice {
    /**
     * 기본 탈출구 : 실패함
     *
     * @param request http 요청
     * @param e 예외 객체
     * @return ResponseService 클래스에 실패 정보 설정
     */
    @ExceptionHandler(Exception.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    protected CommonResult defaultException(HttpServletRequest request, Exception e) {
        return ResponseService.getFailResult(ResultCode.FAILED);
    }

    /**
     * 커스텀 탈출구 : 데이터가 존재하지 않음
     *
     * @param request http 요청
     * @param e 예외 객체
     * @return ResponseService 클래스에 실패 정보 설정
     */
    @ExceptionHandler(CMissingDataException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    protected CommonResult customException(HttpServletRequest request, CMissingDataException e) {
        return ResponseService.getFailResult(ResultCode.MISSING_DATA);
    }

    @ExceptionHandler(CNoManufacturerDataException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    protected CommonResult customException(HttpServletRequest request, CNoManufacturerDataException e) {
        return ResponseService.getFailResult(ResultCode.NO_MANUFACTURER_DATA);
    }
}

