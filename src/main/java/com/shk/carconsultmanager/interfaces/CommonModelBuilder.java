package com.shk.carconsultmanager.interfaces;

public interface CommonModelBuilder<T> {
    T build(); // 할일을 정해주는것
}
