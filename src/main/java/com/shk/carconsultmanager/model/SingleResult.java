package com.shk.carconsultmanager.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class SingleResult<T> extends CommonResult{ // <T>: type의 약자
    private T data; // 데이터를 저장할 변수
}
