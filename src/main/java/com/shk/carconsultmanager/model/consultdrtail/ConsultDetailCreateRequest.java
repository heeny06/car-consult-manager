package com.shk.carconsultmanager.model.consultdrtail;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotNull;

@Getter
@Setter
public class ConsultDetailCreateRequest {

    @ApiModelProperty(notes = "차량트림시퀀스", required = true)
    @NotNull
    private Long carTrimId;

    @ApiModelProperty(notes = "상담고객이름", required = true)
    @NotNull
    @Length(min = 2, max = 20)
    private String consultCustomerName;

    @ApiModelProperty(notes = "상담고객연락처", required = true)
    @NotNull
    @Length(min = 13, max = 20)
    private String consultCustomerPhone;
}
