package com.shk.carconsultmanager.model.consultdrtail;

import com.shk.carconsultmanager.entity.ConsultDetail;
import com.shk.carconsultmanager.interfaces.CommonModelBuilder;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class ConsultDetailItem {

    private Long consultDetailId;

    private String consultCustomerName;

    private String consultCustomerPhone;

    private LocalDateTime dateRequest;

    private Long carTrimId;

    private String carFullName;

    private ConsultDetailItem(ConsultDetailItemBuilder builder) {
        this.consultDetailId = builder.consultDetailId;
        this.consultCustomerName = builder.consultCustomerName;
        this.consultCustomerPhone = builder.consultCustomerPhone;
        this.dateRequest = builder.dateRequest;
        this.carTrimId = builder.carTrimId;
        this.carFullName = builder.carFullName;
    }

    public static class ConsultDetailItemBuilder implements CommonModelBuilder<ConsultDetailItem> {

        private final Long consultDetailId;
        private final String consultCustomerName;
        private final String consultCustomerPhone;
        private final LocalDateTime dateRequest;
        private final Long carTrimId;
        private final String carFullName;

        public ConsultDetailItemBuilder(ConsultDetail consultDetail) {
            this.consultDetailId = consultDetail.getId();
            this.consultCustomerName = consultDetail.getConsultCustomerName();
            this.consultCustomerPhone = consultDetail.getConsultCustomerPhone();
            this.dateRequest = consultDetail.getDateRequest();
            this.carTrimId = consultDetail.getCarTrim().getId();
            this.carFullName = consultDetail.getCarTrim().getCarGrade().getCarModel().getManufacturer().getManufacturerName()
                    + " " + consultDetail.getCarTrim().getCarGrade().getCarModel().getCarModelName();
        }

        @Override
        public ConsultDetailItem build() {

            return new ConsultDetailItem(this);
        }
    }
}
