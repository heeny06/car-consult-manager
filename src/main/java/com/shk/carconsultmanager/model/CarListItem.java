package com.shk.carconsultmanager.model;

import com.shk.carconsultmanager.entity.CarModel;
import com.shk.carconsultmanager.interfaces.CommonModelBuilder;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class CarListItem {

    private Long modelId;

    private String carReleaseStatusName;

    private String carModelImgUrl;

    private String carFullName;

    private CarListItem(CarListItemBuilder builder) {
        this.modelId = builder.modelId;
        this.carModelImgUrl = builder.carModelImgUrl;
        this.carReleaseStatusName = builder.carReleaseStatusName;
        this.carFullName = builder.carFullName;
    }

    public static class CarListItemBuilder implements CommonModelBuilder<CarListItem> {

        private final Long modelId;
        private final String carReleaseStatusName;
        private final String carModelImgUrl;
        private final String carFullName;

        public CarListItemBuilder(CarModel carModel) {
            this.modelId = carModel.getId();
            this.carReleaseStatusName = carModel.getCarReleaseStatus().getName();
            this.carModelImgUrl = carModel.getCarModelImgUrl();
            this.carFullName = carModel.getManufacturer().getManufacturerName() + " " + carModel.getCarModelName();
        }

        @Override
        public CarListItem build() {
            return new CarListItem(this);
        }
    }
}
