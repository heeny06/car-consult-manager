package com.shk.carconsultmanager.model.manufacturer;

import com.shk.carconsultmanager.entity.Manufacturer;
import com.shk.carconsultmanager.interfaces.CommonModelBuilder;
import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class ManufacturerItem {

    @ApiModelProperty(notes = "제조사시퀀스")
    private Long id;

    @ApiModelProperty(notes = "제조사이름")
    private String manufacturerName;

    @ApiModelProperty(notes = "로고이미지파일주소")
    private String logoFileUrl;

    private ManufacturerItem(ManufacturerItemBuilder builder) {
        this.id = builder.id;
        this.manufacturerName = builder.manufacturerName;
        this.logoFileUrl = builder.logoFileUrl;
    }

    public static class ManufacturerItemBuilder implements CommonModelBuilder<ManufacturerItem> {

        private final Long id;
        private final String manufacturerName;
        private final String logoFileUrl;

        public ManufacturerItemBuilder(Manufacturer manufacturer) {
            this.id = manufacturer.getId();
            this.manufacturerName = manufacturer.getManufacturerName();
            this.logoFileUrl = manufacturer.getLogoFileUrl();
        }

        @Override
        public ManufacturerItem build() {
            return new ManufacturerItem(this);
        }
    }
}
