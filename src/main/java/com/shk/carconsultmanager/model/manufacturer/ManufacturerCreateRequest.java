package com.shk.carconsultmanager.model.manufacturer;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotNull;

@Getter
@Setter
public class ManufacturerCreateRequest {

    @ApiModelProperty(notes = "제조사이름")
    @NotNull
    @Length(max = 15)
    private String manufacturerName;

    @ApiModelProperty(notes = "로고파일이름")
    @NotNull
    @Length(max = 150)
    private String logoFileUrl;
}
