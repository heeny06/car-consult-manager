package com.shk.carconsultmanager.model.cartrim;

import com.shk.carconsultmanager.enums.DriveMethod;
import com.shk.carconsultmanager.enums.FuelType;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.validation.constraints.NotNull;

@Getter
@Setter
public class CarTrimCreateRequest {

    @ApiModelProperty(notes = "차량등급시퀀스", required = true)
    @NotNull
    private Long carGradeId;

    @ApiModelProperty(notes = "차량트림이름", required = true)
    @NotNull
    @Length(max = 30)
    private String carTrimName;

    @ApiModelProperty(notes = "차량가격", required = true)
    @NotNull
    private Double carPrice;

    @ApiModelProperty(notes = "연료종류", required = true)
    @NotNull
    @Enumerated(value = EnumType.STRING)
    private FuelType fuelType;

    @ApiModelProperty(notes = "배기량", required = true)
    @NotNull
    private Integer displacement;

    @ApiModelProperty(notes = "구동방식", required = true)
    @NotNull
    @Enumerated(value = EnumType.STRING)
    private DriveMethod driveMethod;

    @ApiModelProperty(notes = "최고출력", required = true)
    @NotNull
    private Integer highestOutput;

    @ApiModelProperty(notes = "최대토크", required = true)
    @NotNull
    private Double maximumTorque;

    @ApiModelProperty(notes = "복합연비", required = true)
    @NotNull
    private Double combMileage;

    @ApiModelProperty(notes = "도심연비", required = true)
    @NotNull
    private Double cityMileage;

    @ApiModelProperty(notes = "고속도로연비", required = true)
    @NotNull
    private Double highwayMileage;

    @ApiModelProperty(notes = "연비등급", required = true)
    @NotNull
    private Short ratingMileage;

    @ApiModelProperty(notes = "CO2배출량", required = true)
    @NotNull
    private Double emissionsCo2;

    @ApiModelProperty(notes = "저공해등급", required = true)
    @NotNull
    private Short lowPollutionRating;

    @ApiModelProperty(notes = "공차중량", required = true)
    @NotNull
    private Double emptyWeight;

    @ApiModelProperty(notes = "최고속력", required = true)
    @NotNull
    private Integer highestSpeed;

    @ApiModelProperty(notes = "제로백", required = true)
    @NotNull
    private Double zeroBack;

    @ApiModelProperty(notes = "자율주행레벨", required = true)
    @NotNull
    private Short freeDrivingLevel;

    @ApiModelProperty(notes = "차량정원", required = true)
    @NotNull
    private Integer carCapacity;
}
