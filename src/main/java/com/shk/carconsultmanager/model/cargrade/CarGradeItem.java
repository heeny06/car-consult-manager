package com.shk.carconsultmanager.model.cargrade;

import com.shk.carconsultmanager.entity.CarGrade;
import com.shk.carconsultmanager.interfaces.CommonModelBuilder;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class CarGradeItem {

    private Long carGradeId;

    private String carGradeName;

    private String carEngineType;

    private Long carModelId;

    private String carModelFullName;

    private CarGradeItem(CarGradeItemBuilder builder) {
        this.carGradeId = builder.carGradeId;
        this.carGradeName = builder.carGradeName;
        this.carEngineType = builder.carEngineType;
        this.carModelId = builder.carModelId;
        this.carModelFullName = builder.carModelFullName;
    }

    public static class CarGradeItemBuilder implements CommonModelBuilder<CarGradeItem> {

        private final Long carGradeId;
        private final String carGradeName;
        private final String carEngineType;
        private final Long carModelId;
        private final String carModelFullName;

        public CarGradeItemBuilder(CarGrade carGrade) {
            this.carGradeId = carGrade.getId();
            this.carGradeName = carGrade.getCarGradeName();
            this.carEngineType = carGrade.getCarEngineType();
            this.carModelId = carGrade.getCarModel().getId();
            this.carModelFullName = carGrade.getCarModel().getManufacturer().getManufacturerName() + " " + carGrade.getCarModel().getCarModelName();
        }

        @Override
        public CarGradeItem build() {
            return new CarGradeItem(this);
        }
    }
}
