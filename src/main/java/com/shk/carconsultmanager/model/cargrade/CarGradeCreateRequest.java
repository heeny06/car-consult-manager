package com.shk.carconsultmanager.model.cargrade;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotNull;

@Getter
@Setter
public class CarGradeCreateRequest {

    @ApiModelProperty(value = "차량모델시퀀스", required = true)
    @NotNull
    private Long carModelId;

    @ApiModelProperty(value = "차량등급이름", required = true)
    @NotNull
    @Length(max = 25)
    private String carGradeName;

    @ApiModelProperty(value = "차량엔진형식", required = true)
    @NotNull
    @Length(max = 15)
    private String carEngineType;
}
