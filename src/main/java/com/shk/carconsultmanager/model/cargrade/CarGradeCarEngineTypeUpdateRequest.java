package com.shk.carconsultmanager.model.cargrade;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotNull;

@Getter
@Setter
public class CarGradeCarEngineTypeUpdateRequest {

    @ApiModelProperty(notes = "차량엔진형식", required = true)
    @NotNull
    @Length(max = 15)
    private String carEngineType;
}
