package com.shk.carconsultmanager.model.carmodel;

import com.shk.carconsultmanager.enums.CarExterior;
import com.shk.carconsultmanager.enums.CarReleaseStatus;
import com.shk.carconsultmanager.enums.CarSize;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.validation.constraints.NotNull;

@Getter
@Setter
public class CarModelCreateRequest {

    @ApiModelProperty(notes = "제조사시퀀스", required = true)
    @NotNull
    private Long manufacturerId;

    @ApiModelProperty(notes = "차량모델 이미지", required = true)
    @NotNull
    @Length(max = 150)
    private String carModelImgUrl;

    @ApiModelProperty(notes = "차량모델이름", required = true)
    @NotNull
    @Length(max = 40)
    private String carModelName;

    @ApiModelProperty(notes = "차량세대", required = true)
    @NotNull
    private Short carGeneration;

    @ApiModelProperty(notes = "연식", required = true)
    @NotNull
    private Short yearType;

    @ApiModelProperty(notes = "차량출시상태", required = true)
    @NotNull
    @Enumerated(value = EnumType.STRING)
    private CarReleaseStatus carReleaseStatus;

    @ApiModelProperty(notes = "차량외장", required = true)
    @NotNull
    @Enumerated(value = EnumType.STRING)
    private CarExterior carExterior;

    @ApiModelProperty(notes = "차량크기", required = true)
    @NotNull
    @Enumerated(value = EnumType.STRING)
    private CarSize carSize;
}
