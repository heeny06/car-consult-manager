package com.shk.carconsultmanager.model.carmodel;

import com.shk.carconsultmanager.entity.CarModel;
import com.shk.carconsultmanager.interfaces.CommonModelBuilder;
import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class CarModelItem {

    @ApiModelProperty(notes = "차량 모델 시퀀스")
    private Long carModelId;

    @ApiModelProperty(notes = "차량 모델 이름")
    private String carModelName;

    @ApiModelProperty(notes = "차량 세대")
    private Short carGeneration;

    @ApiModelProperty(notes = "차량 출시 상태")
    private String carReleaseStatusName;

    @ApiModelProperty(notes = "차량 외장")
    private String carExteriorName;

    @ApiModelProperty(notes = "차량 크기")
    private String carSizeName;

    @ApiModelProperty(notes = "제조사시퀀스")
    private Long manufacturerId;

    @ApiModelProperty(notes = "제조사이름")
    private String manufacturerName;

    private CarModelItem(CarModelItemBuilder builder) {
        this.carModelId = builder.carModelId;
        this.carModelName = builder.carModelName;
        this.carGeneration = builder.carGeneration;
        this.carReleaseStatusName = builder.carReleaseStatusName;
        this.carExteriorName = builder.carExteriorName;
        this.carSizeName = builder.carSizeName;
        this.manufacturerId = builder.manufacturerId;
        this.manufacturerName = builder.manufacturerName;
    }

    public static class CarModelItemBuilder implements CommonModelBuilder<CarModelItem> {

        private final Long carModelId;
        private final String carModelName;
        private final Short carGeneration;
        private final String carReleaseStatusName;
        private final String carExteriorName;
        private final String carSizeName;
        private final Long manufacturerId;
        private final String manufacturerName;

        public CarModelItemBuilder(CarModel carModel) {
            this.carModelId = carModel.getId();
            this.carModelName = carModel.getCarModelName();
            this.carGeneration = carModel.getCarGeneration();
            this.carReleaseStatusName = carModel.getCarReleaseStatus().getName();
            this.carExteriorName = carModel.getCarExterior().getName();
            this.carSizeName = carModel.getCarSize().getName();
            this.manufacturerId = carModel.getManufacturer().getId();
            this.manufacturerName = carModel.getManufacturer().getManufacturerName();
        }

        @Override
        public CarModelItem build() {
            return new CarModelItem(this);
        }
    }
}
