package com.shk.carconsultmanager.model.carmodel;

import com.shk.carconsultmanager.enums.CarReleaseStatus;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.validation.constraints.NotNull;

@Getter
@Setter
public class CarModelReleaseStatusUpdateRequest {

    @ApiModelProperty(notes = "차량출시상태", required = true)
    @NotNull
    @Enumerated(value = EnumType.STRING)
    private CarReleaseStatus carReleaseStatus;
}
