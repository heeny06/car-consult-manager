package com.shk.carconsultmanager.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CarInfoDetailRangeItem {

    private Double maxDoubleValue;

    private Double minDoubleValue;

    private Integer maxIntValue;

    private Integer minIntValue;
}
