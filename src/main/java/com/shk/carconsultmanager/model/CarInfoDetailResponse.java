package com.shk.carconsultmanager.model;

import com.shk.carconsultmanager.entity.CarModel;
import com.shk.carconsultmanager.interfaces.CommonModelBuilder;
import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.util.List;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class CarInfoDetailResponse {

    @ApiModelProperty(notes = "차량전체이름")
    private String carFullName;

    @ApiModelProperty(notes = "차량이미지url")
    private String carModelImgUrl;

    @ApiModelProperty(notes = "차량가격")
    private CarInfoDetailRangeItem carPrice;

    @ApiModelProperty(notes = "차량외장")
    private String carType;

    @ApiModelProperty(notes = "차량연료종료")
    private List<String> carFuelTypesNames;

    @ApiModelProperty(notes = "차량배기량")
    private CarInfoDetailRangeItem displacement;

    @ApiModelProperty(notes = "차량연비")
    private CarInfoDetailRangeItem carMileage;

    @ApiModelProperty(notes = "차량정원")
    private CarInfoDetailRangeItem carCapacity;

    private CarInfoDetailResponse(CarInfoDetailResponseBuilder builder) {
        this.carFullName = builder.carFullName;
        this.carModelImgUrl = builder.carModelImgUrl;
        this.carPrice = builder.carPrice;
        this.carType = builder.carType;
        this.carFuelTypesNames = builder.carFuelTypesNames;
        this.displacement = builder.displacement;
        this.carMileage = builder.carMileage;
        this.carCapacity = builder.carCapacity;
    }

    public static class CarInfoDetailResponseBuilder implements CommonModelBuilder<CarInfoDetailResponse> {

        private final String carFullName;
        private final String carModelImgUrl;
        private final CarInfoDetailRangeItem carPrice;
        private final String carType;
        private final List<String> carFuelTypesNames;
        private final CarInfoDetailRangeItem displacement;
        private final CarInfoDetailRangeItem carMileage;
        private final CarInfoDetailRangeItem carCapacity;

        public CarInfoDetailResponseBuilder(
                CarModel carModel,
                Double minPrice,
                Double maxPrice,
                List<String> carFuelTypesNames,
                Integer minDisplacement,
                Integer maxDisplacement,
                Double minCarMileage,
                Double maxCarMileage,
                Integer minCarCapacity,
                Integer maxCarCapacity
        ) {
            this.carFullName = carModel.getManufacturer().getManufacturerName() + " " +carModel.getCarModelName();
            this.carModelImgUrl = carModel.getCarModelImgUrl();

            CarInfoDetailRangeItem carPrice = new CarInfoDetailRangeItem();
            carPrice.setMaxDoubleValue(maxPrice);
            carPrice.setMinDoubleValue(minPrice);
            this.carPrice = carPrice;

            this.carType = carModel.getCarExterior().getName() + " (" + carModel.getCarSize().getName() + ")";

            this.carFuelTypesNames = carFuelTypesNames;

            CarInfoDetailRangeItem displacement = new CarInfoDetailRangeItem();
            displacement.setMinIntValue(minDisplacement);
            displacement.setMaxIntValue(maxDisplacement);
            this.displacement = displacement;

            CarInfoDetailRangeItem carMileage = new CarInfoDetailRangeItem();
            carMileage.setMaxDoubleValue(maxCarMileage);
            carMileage.setMinDoubleValue(minCarMileage);
            this.carMileage = carMileage;

            CarInfoDetailRangeItem carCapacity = new CarInfoDetailRangeItem();
            carCapacity.setMinIntValue(minCarCapacity);
            carCapacity.setMaxIntValue(maxCarCapacity);
            this.carCapacity = carCapacity;
        }

        @Override
        public CarInfoDetailResponse build() {

            return new CarInfoDetailResponse(this);
        }
    }
}
