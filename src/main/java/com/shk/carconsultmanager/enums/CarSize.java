package com.shk.carconsultmanager.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum CarSize {
    COMPACT("경차")
    , SMALL("소형")
    , MIDSIZE("중형")
    , LARGE("대형")
    ;

    private final String name;
}
