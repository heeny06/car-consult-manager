package com.shk.carconsultmanager.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum DriveMethod {
    FF
    , FR
    , FMR
    , RMR
    , RR
    , WD4
    ;
}
