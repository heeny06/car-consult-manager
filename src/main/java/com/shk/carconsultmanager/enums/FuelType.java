package com.shk.carconsultmanager.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum FuelType {
    GASOLINE("가솔린")
    , DIESEL("디젤")
    , LPG("LPG")
    , ELECTRON("전기")
    , HYBRID("하이브리드")
    ;

    private final String name;
}
