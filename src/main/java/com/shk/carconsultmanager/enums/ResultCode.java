package com.shk.carconsultmanager.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum ResultCode { // 결과코드를 관리
    SUCCESS(0, "성공하였습니다.") // 성공은 하나인데 실패하는 유형은 여러가지
    , FAILED(-1, "실패하였습니다.")

    , MISSING_DATA(-10000, "데이터를 찾을 수 없습니다.")

    , NO_MANUFACTURER_DATA(-20000, "제조사 정보가 없습니다.")
    ;

    private final Integer code; // 코드의 자료 형식과 이름
    private final String msg; // 메시지의 자료 형식과 이름
}
