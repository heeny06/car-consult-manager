package com.shk.carconsultmanager.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum CarReleaseStatus {
    ON_SALE("시판")
    , DUE("예정")
    , DISCONTINUED("단종")
    , UNDETERMINED("미정")
    ;

    private final String name;
}
