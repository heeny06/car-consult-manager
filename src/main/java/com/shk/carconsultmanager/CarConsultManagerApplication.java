package com.shk.carconsultmanager;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CarConsultManagerApplication {

    public static void main(String[] args) {
        SpringApplication.run(CarConsultManagerApplication.class, args);
    }

}
