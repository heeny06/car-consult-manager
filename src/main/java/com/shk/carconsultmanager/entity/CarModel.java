package com.shk.carconsultmanager.entity;

import com.shk.carconsultmanager.enums.CarExterior;
import com.shk.carconsultmanager.enums.CarReleaseStatus;
import com.shk.carconsultmanager.enums.CarSize;
import com.shk.carconsultmanager.interfaces.CommonModelBuilder;
import com.shk.carconsultmanager.model.carmodel.CarModelCreateRequest;
import com.shk.carconsultmanager.model.carmodel.CarModelReleaseStatusUpdateRequest;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class CarModel {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "manufacturerId", nullable = false)
    private Manufacturer manufacturer;

    @Column(nullable = false, length = 200)
    private String carModelImgUrl;

    @Column(nullable = false, length = 40)
    private String carModelName;

    @Column(nullable = false)
    private Short carGeneration;

    @Column(nullable = false)
    private Short yearType;

    @Column(nullable = false, length = 15)
    @Enumerated(value = EnumType.STRING)
    private CarReleaseStatus carReleaseStatus;

    @Column(nullable = false, length = 15)
    @Enumerated(value = EnumType.STRING)
    private CarExterior carExterior;

    @Column(nullable = false, length = 10)
    @Enumerated(value = EnumType.STRING)
    private CarSize carSize;

    @Column(nullable = false)
    private LocalDateTime dateCreate;

    @Column(nullable = false)
    private LocalDateTime dateUpdate;

    public void putCarReleaseStatus(CarModelReleaseStatusUpdateRequest updateRequest) {
        this.carReleaseStatus = updateRequest.getCarReleaseStatus();
        this.dateUpdate = LocalDateTime.now();
    }

    private CarModel(CarModelBuilder builder) {
        this.manufacturer = builder.manufacturer;
        this.carModelImgUrl = builder.carModelImgUrl;
        this.carModelName = builder.carModelName;
        this.carGeneration = builder.carGeneration;
        this.yearType = builder.yearType;
        this.carReleaseStatus = builder.carReleaseStatus;
        this.carExterior = builder.carExterior;
        this.carSize = builder.carSize;
        this.dateCreate = builder.dateCreate;
        this.dateUpdate = builder.dateUpdate;
    }

    public static class CarModelBuilder implements CommonModelBuilder<CarModel> {

        private final Manufacturer manufacturer;
        private final String carModelImgUrl;
        private final String carModelName;
        private final Short carGeneration;
        private final Short yearType;
        private final CarReleaseStatus carReleaseStatus;
        private final CarExterior carExterior;
        private final CarSize carSize;
        private final LocalDateTime dateCreate;
        private final LocalDateTime dateUpdate;

        public CarModelBuilder(Manufacturer manufacturer, CarModelCreateRequest createRequest) {
            this.manufacturer = manufacturer;
            this.carModelImgUrl = createRequest.getCarModelImgUrl();
            this.carModelName = createRequest.getCarModelName();
            this.carGeneration = createRequest.getCarGeneration();
            this.yearType = createRequest.getYearType();
            this.carReleaseStatus = createRequest.getCarReleaseStatus();
            this.carExterior = createRequest.getCarExterior();
            this.carSize = createRequest.getCarSize();
            this.dateCreate = LocalDateTime.now();
            this.dateUpdate = LocalDateTime.now();
        }

        @Override
        public CarModel build() {

            return new CarModel(this);
        }
    }
}
