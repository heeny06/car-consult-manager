package com.shk.carconsultmanager.entity;

import com.shk.carconsultmanager.interfaces.CommonModelBuilder;
import com.shk.carconsultmanager.model.consultdrtail.ConsultDetailCreateRequest;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class ConsultDetail {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "carTrimId", nullable = false)
    private CarTrim carTrim;

    @Column(nullable = false, length = 20)
    private String consultCustomerName;

    @Column(nullable = false, length = 20)
    private String consultCustomerPhone;

    @Column(nullable = false)
    private LocalDateTime dateRequest;

    private ConsultDetail(ConsultDetailBuilder builder) {
        this.carTrim = builder.carTrim;
        this.consultCustomerName = builder.consultCustomerName;
        this.consultCustomerPhone = builder.consultCustomerPhone;
        this.dateRequest = builder.dateRequest;
    }

    public static class ConsultDetailBuilder implements CommonModelBuilder<ConsultDetail> {

        private final CarTrim carTrim;
        private final String consultCustomerName;
        private final String consultCustomerPhone;
        private final LocalDateTime dateRequest;

        public ConsultDetailBuilder(CarTrim carTrim, ConsultDetailCreateRequest createRequest) {
            this.carTrim = carTrim;
            this.consultCustomerName = createRequest.getConsultCustomerName();
            this.consultCustomerPhone = createRequest.getConsultCustomerPhone();
            this.dateRequest = LocalDateTime.now();
        }

        @Override
        public ConsultDetail build() {
            return new ConsultDetail(this);
        }
    }
}
