package com.shk.carconsultmanager.entity;

import com.shk.carconsultmanager.interfaces.CommonModelBuilder;
import com.shk.carconsultmanager.model.cargrade.CarGradeCarEngineTypeUpdateRequest;
import com.shk.carconsultmanager.model.cargrade.CarGradeCreateRequest;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class CarGrade {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "carModelId", nullable = false)
    private CarModel carModel;

    @Column(nullable = false, length = 25)
    private String carGradeName;

    @Column(nullable = false, length = 15)
    private String carEngineType;

    @Column(nullable = false)
    private LocalDateTime dateCreate;

    @Column(nullable = false)
    private LocalDateTime dateUpdate;

    public void putCarEngineType(CarGradeCarEngineTypeUpdateRequest updateRequest) {
        this.carEngineType = updateRequest.getCarEngineType();
        this.dateUpdate = LocalDateTime.now();
    }

    private CarGrade(CarGradeBuilder builder) {
        this.carModel = builder.carModel;
        this.carGradeName = builder.carGradeName;
        this.carEngineType = builder.carEngineType;
        this.dateCreate = builder.dateCreate;
        this.dateUpdate = builder.dateUpdate;
    }

    public static class CarGradeBuilder implements CommonModelBuilder<CarGrade> {

        private final CarModel carModel;
        private final String carGradeName;
        private final String carEngineType;
        private final LocalDateTime dateCreate;
        private final LocalDateTime dateUpdate;

        public CarGradeBuilder(CarModel carModel, CarGradeCreateRequest createRequest) {
            this.carModel = carModel;
            this.carGradeName = createRequest.getCarGradeName();
            this.carEngineType = createRequest.getCarEngineType();
            this.dateCreate = LocalDateTime.now();
            this.dateUpdate = LocalDateTime.now();
        }

        @Override
        public CarGrade build() {
            return new CarGrade(this);
        }
    }
}
