package com.shk.carconsultmanager.entity;

import com.shk.carconsultmanager.enums.DriveMethod;
import com.shk.carconsultmanager.enums.FuelType;
import com.shk.carconsultmanager.interfaces.CommonModelBuilder;
import com.shk.carconsultmanager.model.cartrim.CarTrimCreateRequest;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDateTime;


@Entity
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class CarTrim {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "carGradeId", nullable = false)
    private CarGrade carGrade;

    @Column(nullable = false, length = 30)
    private String carTrimName;

    @Column(nullable = false)
    private Double carPrice;

    @Column(nullable = false, length = 10)
    @Enumerated(value = EnumType.STRING)
    private FuelType fuelType;

    @Column(nullable = false)
    private Integer displacement;

    @Column(nullable = false, length = 15)
    private String gearbox;

    @Column(nullable = false, length = 5)
    @Enumerated(value = EnumType.STRING)
    private DriveMethod driveMethod;

    @Column(nullable = false)
    private Integer highestOutput;

    @Column(nullable = false)
    private Double maximumTorque;

    @Column(nullable = false)
    private Double combMileage;

    @Column(nullable = false)
    private Double cityMileage;

    @Column(nullable = false)
    private Double highwayMileage;

    @Column(nullable = false)
    private Short ratingMileage;

    @Column(nullable = false)
    private Double emissionsCo2;

    @Column(nullable = false)
    private Short lowPollutionRating;

    @Column(nullable = false)
    private Double emptyWeight;

    @Column(nullable = false)
    private Integer highestSpeed;

    @Column(nullable = false)
    private Double zeroBack;

    @Column(nullable = false)
    private Short freeDrivingLevel;

    @Column(nullable = false)
    private Integer carCapacity;

    @Column(nullable = false)
    private LocalDateTime dateCreate;

    @Column(nullable = false)
    private LocalDateTime dateUpdate;

    private CarTrim(CarTrimBuilder builder) {
        this.carGrade = builder.carGrade;
        this.carTrimName = builder.carTrimName;
        this.carPrice = builder.carPrice;
        this.fuelType = builder.fuelType;
        this.displacement = builder.displacement;
        this.gearbox = builder.gearbox;
        this.driveMethod = builder.driveMethod;
        this.highestOutput = builder.highestOutput;
        this.maximumTorque = builder.maximumTorque;
        this.combMileage = builder.combMileage;
        this.cityMileage = builder.cityMileage;
        this.highwayMileage = builder.highwayMileage;
        this.ratingMileage = builder.ratingMileage;
        this.emissionsCo2 = builder.emissionsCo2;
        this.lowPollutionRating = builder.lowPollutionRating;
        this.emptyWeight = builder.emptyWeight;
        this.highestSpeed = builder.highestSpeed;
        this.zeroBack = builder.zeroBack;
        this.freeDrivingLevel = builder.freeDrivingLevel;
        this.carCapacity = builder.carCapacity;
        this.dateCreate = builder.dateCreate;
        this.dateUpdate = builder.dateUpdate;
    }

    public static class CarTrimBuilder implements CommonModelBuilder<CarTrim> {

        private final CarGrade carGrade;
        private final String carTrimName;
        private final Double carPrice;
        private final FuelType fuelType;
        private final Integer displacement;
        private final String gearbox;
        private final DriveMethod driveMethod;
        private final Integer highestOutput;
        private final Double maximumTorque;
        private final Double combMileage;
        private final Double cityMileage;
        private final Double highwayMileage;
        private final Short ratingMileage;
        private final Double emissionsCo2;
        private final Short lowPollutionRating;
        private final Double emptyWeight;
        private final Integer highestSpeed;
        private final Double zeroBack;
        private final Short freeDrivingLevel;
        private final Integer carCapacity;
        private final LocalDateTime dateCreate;
        private final LocalDateTime dateUpdate;

        public CarTrimBuilder(CarGrade carGrade, CarTrimCreateRequest createRequest) {
            this.carGrade = carGrade;
            this.carTrimName = createRequest.getCarTrimName();
            this.carPrice = createRequest.getCarPrice();
            this.fuelType = createRequest.getFuelType();
            this.displacement = createRequest.getDisplacement();
            this.gearbox = createRequest.getCarTrimName();
            this.driveMethod = createRequest.getDriveMethod();
            this.highestOutput = createRequest.getHighestOutput();
            this.maximumTorque = createRequest.getMaximumTorque();
            this.combMileage = createRequest.getCombMileage();
            this.cityMileage = createRequest.getCityMileage();
            this.highwayMileage = createRequest.getHighwayMileage();
            this.ratingMileage = createRequest.getRatingMileage();
            this.emissionsCo2 = createRequest.getEmissionsCo2();
            this.lowPollutionRating = createRequest.getLowPollutionRating();
            this.emptyWeight = createRequest.getEmptyWeight();
            this.highestSpeed = createRequest.getHighestSpeed();
            this.zeroBack = createRequest.getZeroBack();
            this.freeDrivingLevel = createRequest.getFreeDrivingLevel();
            this.carCapacity = createRequest.getCarCapacity();
            this.dateCreate = LocalDateTime.now();
            this.dateUpdate = LocalDateTime.now();
        }

        @Override
        public CarTrim build() {
            return new CarTrim(this);
        }
    }


}
