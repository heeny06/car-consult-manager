package com.shk.carconsultmanager.entity;

import com.shk.carconsultmanager.interfaces.CommonModelBuilder;
import com.shk.carconsultmanager.model.manufacturer.ManufacturerCreateRequest;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class Manufacturer {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false, length = 15)
    private String manufacturerName;

    @Column(nullable = false, length = 150)
    private String logoFileUrl;

    private Manufacturer(ManufacturerBuilder builder) {
        this.manufacturerName = builder.manufacturerName;
        this.logoFileUrl = builder.logoFileUrl;
    }

    public static class ManufacturerBuilder implements CommonModelBuilder<Manufacturer> {

        private final String manufacturerName;
        private final String logoFileUrl;

        public ManufacturerBuilder(ManufacturerCreateRequest createRequest) {
            this.manufacturerName = createRequest.getManufacturerName();
            this.logoFileUrl = createRequest.getLogoFileUrl();
        }

        @Override
        public Manufacturer build() {

            return new Manufacturer(this);
        }
    }
}
