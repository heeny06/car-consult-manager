package com.shk.carconsultmanager.service;

import com.shk.carconsultmanager.entity.CarTrim;
import com.shk.carconsultmanager.entity.ConsultDetail;
import com.shk.carconsultmanager.model.ListResult;
import com.shk.carconsultmanager.model.consultdrtail.ConsultDetailCreateRequest;
import com.shk.carconsultmanager.model.consultdrtail.ConsultDetailItem;
import com.shk.carconsultmanager.repository.ConsultDetailRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class ConsultDetailService {

    private final ConsultDetailRepository consultDetailRepository;

    /**
     * 상담내역 등록
     *
     * @param carTrim 차량 트림 정보
     * @param createRequest 등록할 상담내역 정보
     */
    public void setConsultDetail(CarTrim carTrim, ConsultDetailCreateRequest createRequest) {
        ConsultDetail consultDetail = new ConsultDetail.ConsultDetailBuilder(carTrim, createRequest).build();
        consultDetailRepository.save(consultDetail);
    }

    /**
     * 상담내역 정보 리스트 불러오
     *
     * @param dateStart 조회 시작날
     * @param dateEnd 조회 마지막날
     * @return 상담내역 정보 리스트
     */
    public ListResult<ConsultDetailItem> getConsultDetails(LocalDate dateStart, LocalDate dateEnd) {
        LocalDateTime dateStartTime = LocalDateTime.of(
                dateStart.getYear(),
                dateStart.getMonthValue(),
                dateStart.getDayOfMonth(),
                0,
                0,
                0
        );

        LocalDateTime dateEndTime = LocalDateTime.of(
                dateEnd.getYear(),
                dateEnd.getMonthValue(),
                dateEnd.getDayOfMonth(),
                23,
                59,
                59
        );

        List<ConsultDetail> consultDetails = consultDetailRepository.findAllByDateRequestGreaterThanEqualAndDateRequestLessThanEqualOrderByIdDesc(
                dateStartTime,
                dateEndTime
        );

        List<ConsultDetailItem> result = new LinkedList<>();

        consultDetails.forEach(consultDetail -> {
            ConsultDetailItem addItem = new ConsultDetailItem.ConsultDetailItemBuilder(consultDetail).build();
            result.add(addItem);
        });

        return ListConvertService.settingResult(result);
    }
}
