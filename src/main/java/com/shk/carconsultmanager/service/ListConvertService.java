package com.shk.carconsultmanager.service;

import com.shk.carconsultmanager.model.ListResult;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ListConvertService {
    public static PageRequest getPageable(int pageNum) { // 몇번째 페이지를 가져오고 싶니
        return PageRequest.of(pageNum - 1, 10);
    }

    public static PageRequest getPageable(int pageNum, int pageSize) {
        return PageRequest.of(pageNum - 1, pageSize);
    }

    public static <T> ListResult<T> settingResult(List<T> list) { // list 형태의 무언가를 받음
        ListResult<T> result = new ListResult<>();
        result.setList(list);
        result.setTotalItemCount((long)list.size());
        result.setTotalPage(1);
        result.setCurrentPage(1);

        return result;
    }

    public static <T> ListResult<T> settingResult(List<T> list, long totalItemCount, int totalPage, int currentPage) {
        ListResult<T> result = new ListResult<>();
        result.setList(list);
        result.setTotalItemCount(totalItemCount);
        result.setTotalPage(totalPage == 0 ? 1 : totalPage);
        result.setCurrentPage(currentPage + 1);

        return result;
    }
}
