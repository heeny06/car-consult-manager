package com.shk.carconsultmanager.service;

import com.shk.carconsultmanager.entity.CarGrade;
import com.shk.carconsultmanager.entity.CarModel;
import com.shk.carconsultmanager.entity.CarTrim;
import com.shk.carconsultmanager.entity.Manufacturer;
import com.shk.carconsultmanager.exception.CMissingDataException;
import com.shk.carconsultmanager.model.CarInfoDetailResponse;
import com.shk.carconsultmanager.model.CarListItem;
import com.shk.carconsultmanager.model.ListResult;
import com.shk.carconsultmanager.model.cargrade.CarGradeCarEngineTypeUpdateRequest;
import com.shk.carconsultmanager.model.cargrade.CarGradeCreateRequest;
import com.shk.carconsultmanager.model.carmodel.CarModelCreateRequest;
import com.shk.carconsultmanager.model.carmodel.CarModelReleaseStatusUpdateRequest;
import com.shk.carconsultmanager.model.cartrim.CarTrimCreateRequest;
import com.shk.carconsultmanager.model.manufacturer.ManufacturerCreateRequest;
import com.shk.carconsultmanager.repository.CarGradeRepository;
import com.shk.carconsultmanager.repository.CarModelRepository;
import com.shk.carconsultmanager.repository.CarTrimRepository;
import com.shk.carconsultmanager.repository.ManufacturerRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
@RequiredArgsConstructor
public class CarInfoService {

    private final ManufacturerRepository manufacturerRepository;
    private final CarModelRepository carModelRepository;
    private final CarGradeRepository carGradeRepository;
    private final CarTrimRepository carTrimRepository;

    /**
     * 제조사 등록
     *
     * @param createRequest 등록할 제조사 정보
     */
    public void setManufacturer(ManufacturerCreateRequest createRequest) {
        Manufacturer manufacturer = new Manufacturer.ManufacturerBuilder(createRequest).build();
        manufacturerRepository.save(manufacturer);
    }

    /**
     * 차량 모델 등록
     *
     * @param createRequest 등록할 차량 모델 정보
     */
    public void setCarModel(CarModelCreateRequest createRequest) {
        Manufacturer manufacturer = manufacturerRepository.findById(createRequest.getManufacturerId()).orElseThrow(CMissingDataException::new);
        CarModel carModel = new CarModel.CarModelBuilder(manufacturer, createRequest).build();
        carModelRepository.save(carModel);
    }

    /**
     * 차량 등급 등록
     *
     * @param createRequest 등록할 차량 등급 정보
     */
    public void setCarGrade(CarGradeCreateRequest createRequest) {
        CarModel carModel = carModelRepository.findById(createRequest.getCarModelId()).orElseThrow(CMissingDataException::new);
        CarGrade carGrade = new CarGrade.CarGradeBuilder(carModel, createRequest).build();
        carGradeRepository.save(carGrade);
    }

    /**
     * 차량 트림 등록
     *
     * @param createRequest 등록할 차량 트림 정보
     */
    public void setCarTrim(CarTrimCreateRequest createRequest) {
        CarGrade carGrade = carGradeRepository.findById(createRequest.getCarGradeId()).orElseThrow(CMissingDataException::new);
        CarTrim carTrim = new CarTrim.CarTrimBuilder(carGrade, createRequest).build();
        carTrimRepository.save(carTrim);
    }

    /**
     * 차량 트림 원본 정보 가져오기
     *
     * @param trimId 트림 시퀀스
     * @return 차량 트림 원본 정보
     */
    public CarTrim getCarTrimData(long trimId) {
        return carTrimRepository.findById(trimId).orElseThrow(CMissingDataException::new);
    }

    /**
     * 차량 정보 리스트 불러오기
     *
     * @return 차량 정보 리스트
     */
    public ListResult<CarListItem> getCarList() {
        List<CarModel> carModels = carModelRepository.findAll();

        List<CarListItem> result = new LinkedList<>();

        carModels.forEach(carModel -> {
            CarListItem addItem = new CarListItem.CarListItemBuilder(carModel).build();
            result.add(addItem);
        });

        return ListConvertService.settingResult(result);
    }

    /**
     * 차량 상세정보 불러오기
     *
     * @param modelId 차량 모델 시퀀스
     * @return 차량 상세정보
     */
    public CarInfoDetailResponse getCarInfoDetail(long modelId) {
        CarModel carModel = carModelRepository.findById(modelId).orElseThrow(CMissingDataException::new);

        List<CarTrim> trims = carTrimRepository.findAllByCarGrade_CarModel_Id(carModel.getId());

        ArrayList<Double> prices = new ArrayList<>();
        ArrayList<Double> carMileage = new ArrayList<>();
        ArrayList<Integer> carCapacity = new ArrayList<>();
        ArrayList<Integer> displacement = new ArrayList<>();

        List<String> carFuelTypes = new LinkedList<>();

        trims.forEach(trim -> {
            prices.add(trim.getCarPrice());
            carMileage.add(trim.getCombMileage());
            displacement.add(trim.getDisplacement());
            carCapacity.add(trim.getCarCapacity());
            carFuelTypes.add(trim.getFuelType().getName());
        });

        Collections.sort(prices);
        Double minPrice = prices.get(0);
        Double maxPrice = prices.get(prices.size() - 1);

        Set<String> tempSet = new HashSet<>(carFuelTypes);
        List<String> carFuelTypesResult = new LinkedList<>(tempSet);

        Collections.sort(displacement);
        Integer minDisplacement = displacement.get(0);
        Integer maxDisplacement = displacement.get(displacement.size() - 1);

        Collections.sort(carMileage);
        Double minCarMileage = carMileage.get(0);
        Double maxCarMileage = carMileage.get(carMileage.size() - 1);

        Collections.sort(carCapacity);
        Integer minCarCapacity = carCapacity.get(0);
        Integer maxCarCapacity = carCapacity.get(carCapacity.size() - 1);

        return new CarInfoDetailResponse.CarInfoDetailResponseBuilder(
                carModel,
                minPrice,
                maxPrice,
                carFuelTypesResult,
                minDisplacement,
                maxDisplacement,
                minCarMileage,
                maxCarMileage,
                minCarCapacity,
                maxCarCapacity
        ).build();
    }

    /**
     * 차량 모델 출시 상태 정보 변경
     *
     * @param modelId 차량 모델 시퀀스
     * @param updateRequest 변경할 차량 모델 출시 상태 정보
     */
    public void putCarModelReleaseStatus(long modelId, CarModelReleaseStatusUpdateRequest updateRequest) {
        CarModel carModel = carModelRepository.findById(modelId).orElseThrow(CMissingDataException::new);
        carModel.putCarReleaseStatus(updateRequest);

        carModelRepository.save(carModel);
    }

    /**
     * 차량 엔진 종류 정보 변경
     *
     * @param gradeId 차량 등급 시퀀스
     * @param updateRequest 변경할 엔진 종류 정보
     */
    public void putCarGradeCarEngineType(long gradeId, CarGradeCarEngineTypeUpdateRequest updateRequest) {
        CarGrade carGrade = carGradeRepository.findById(gradeId).orElseThrow(CMissingDataException::new);
        carGrade.putCarEngineType(updateRequest);

        carGradeRepository.save(carGrade);
    }
}
