package com.shk.carconsultmanager.service;

import com.shk.carconsultmanager.enums.ResultCode;
import com.shk.carconsultmanager.model.CommonResult;
import com.shk.carconsultmanager.model.ListResult;
import com.shk.carconsultmanager.model.SingleResult;
import org.springframework.stereotype.Service;

@Service // 협업 없음
public class ResponseService {
    public static <T> ListResult<T> getListResult(ListResult<T> result, boolean isSuccess) {
        if (isSuccess) setSuccessResult(result);
        else setFailResult(result);
        return result;
    }

    public static <T> SingleResult<T> getSingleResult(T data) {
        SingleResult<T> result = new SingleResult<>();
        result.setData(data);
        setSuccessResult(result);
        return result;
    }

    public static CommonResult getSuccessResult() { // 성공은 하나라서 ()
        CommonResult result = new CommonResult();
        setSuccessResult(result);
        return result;
    }

    public static CommonResult getFailResult(ResultCode resultCode) { // 어떤 유형의 실패인지
        CommonResult result = new CommonResult();
        result.setIsSuccess(false);
        result.setCode(resultCode.getCode());
        result.setMsg(resultCode.getMsg());
        return result;
    }

    private static void setSuccessResult(CommonResult result) { // static은 자주 쓰는거라서 고정시켰다는 의미
        result.setIsSuccess(true);
        result.setCode(ResultCode.SUCCESS.getCode());
        result.setMsg(ResultCode.SUCCESS.getMsg());
    }

    private static void setFailResult(CommonResult result) {
        result.setIsSuccess(false);
        result.setCode(ResultCode.FAILED.getCode());
        result.setMsg(ResultCode.FAILED.getMsg());
    }
}

