package com.shk.carconsultmanager.controller;

import com.shk.carconsultmanager.entity.CarTrim;
import com.shk.carconsultmanager.model.CommonResult;
import com.shk.carconsultmanager.model.ListResult;
import com.shk.carconsultmanager.model.consultdrtail.ConsultDetailCreateRequest;
import com.shk.carconsultmanager.model.consultdrtail.ConsultDetailItem;
import com.shk.carconsultmanager.service.CarInfoService;
import com.shk.carconsultmanager.service.ConsultDetailService;
import com.shk.carconsultmanager.service.ResponseService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.time.LocalDate;

@Api(tags = "상담내역 정보 관리")
@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/consult-detail")
public class ConsultDetailController {

    private final CarInfoService carInfoService;
    private final ConsultDetailService consultDetailService;

    @ApiOperation(value = "상담내역 정보 등록")
    @PostMapping("/new")
    public CommonResult setConsultDetail(@RequestBody @Valid ConsultDetailCreateRequest createRequest) {
        CarTrim carTrim = carInfoService.getCarTrimData(createRequest.getCarTrimId());
        consultDetailService.setConsultDetail(carTrim, createRequest);

        return ResponseService.getSuccessResult();
    }

    @ApiOperation(value = "상담내역 리스트 조회")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "dateStart", value = "시작날짜", required = true),
            @ApiImplicitParam(name = "dateEnd", value = "끝나는날짜", required = true)
    })
    @GetMapping("/search")
    public ListResult<ConsultDetailItem> getConsultDetails(
            @RequestParam(value = "dateStart") LocalDate dateStart,
            @RequestParam(value = "dateEnd") LocalDate dateEnd
    ) {
        return ResponseService.getListResult(consultDetailService.getConsultDetails(dateStart, dateEnd), true);
    }
}
