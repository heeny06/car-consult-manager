package com.shk.carconsultmanager.controller;

import com.shk.carconsultmanager.model.*;
import com.shk.carconsultmanager.model.cargrade.CarGradeCarEngineTypeUpdateRequest;
import com.shk.carconsultmanager.model.carmodel.CarModelCreateRequest;
import com.shk.carconsultmanager.model.carmodel.CarModelReleaseStatusUpdateRequest;
import com.shk.carconsultmanager.model.cartrim.CarTrimCreateRequest;
import com.shk.carconsultmanager.model.manufacturer.ManufacturerCreateRequest;
import com.shk.carconsultmanager.service.CarInfoService;
import com.shk.carconsultmanager.service.ResponseService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@Api(tags = "차량 정보 관리")
@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/car-info")
public class CarInfoController {

    private final CarInfoService carInfoService;

    @ApiOperation(value = "제조사 정보 등록")
    @PostMapping("/manufacturer/new")
    public CommonResult setManufacturer(@RequestBody @Valid ManufacturerCreateRequest createRequest) {
        carInfoService.setManufacturer(createRequest);

        return ResponseService.getSuccessResult();
    }

    @ApiOperation(value = "차량 모델 정보 등록")
    @PostMapping("/grade/new")
    public CommonResult setCarModel(@RequestBody @Valid CarModelCreateRequest createRequest) {
        carInfoService.setCarModel(createRequest);

        return ResponseService.getSuccessResult();
    }

    @ApiOperation(value = "차량 트림 정보 등록")
    @PostMapping("/trim/new")
    public CommonResult setCarTrim(@RequestBody @Valid CarTrimCreateRequest createRequest) {
        carInfoService.setCarTrim(createRequest);

        return ResponseService.getSuccessResult();
    }

    @ApiOperation(value = "차량 리스트 불러오기")
    @GetMapping("/list")
    public ListResult<CarListItem> getCarList() {
        return ResponseService.getListResult(carInfoService.getCarList(), true);
    }

    @ApiOperation(value = "차량 상세정보 불러오기")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "modelId", value = "차량모델시퀀스", required = true)
    })
    @GetMapping("/model/{modelId}")
    public SingleResult<CarInfoDetailResponse> getCarInfoDetail(@PathVariable long modelId) {
        return ResponseService.getSingleResult(carInfoService.getCarInfoDetail(modelId));
    }

    @ApiOperation(value = "차량 모델 출시 상태 정보 수정")
    @PutMapping("/model/release-status/{carModelId}")
    public CommonResult putCarModelReleaseStatus(@PathVariable long carModelId, @RequestBody @Valid CarModelReleaseStatusUpdateRequest updateRequest) {
        carInfoService.putCarModelReleaseStatus(carModelId, updateRequest);

        return ResponseService.getSuccessResult();
    }

    @ApiOperation(value = "차량 등급 엔진형식 정보 수정")
    @PutMapping("/grade/engine-type/{carGradeId}")
    public CommonResult putCarGradeCarEngineType(@PathVariable long carGradeId, @RequestBody @Valid CarGradeCarEngineTypeUpdateRequest updateRequest) {
        carInfoService.putCarGradeCarEngineType(carGradeId, updateRequest);

        return ResponseService.getSuccessResult();
    }
}
